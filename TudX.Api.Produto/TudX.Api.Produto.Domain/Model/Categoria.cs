﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Core.Model;

namespace TudX.Api.Produto.Domain.Model
{
    public class Categoria : IEntity
    {
        public virtual long Identificador { get; set; }
        public virtual string Nome { get; set; }
        public virtual string Url { get; set; }
        public virtual long? IdentificadorSuperior { get; set; }        
        public virtual ICollection<Categoria> CategoriasFilhas { get; set; }

        public Categoria(string nome)
        {
            Nome = nome;
        }

        public Categoria()
        {

        }
    }
}
