﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Core.Model;

namespace TudX.Api.Produto.Domain.Model
{
    public class TermoBuscaProduto : IEntity
    {
        public virtual long Identificador { get; set; }
        public virtual string Nome { get; set; }
        public virtual DateTime DataBusca { get; set; }
    }
}
