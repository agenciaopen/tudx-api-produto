﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Core.Model;

namespace TudX.Api.Produto.Domain.Model
{
    public class ImagemProduto : IEntity
    {
        public virtual long Identificador { get; set; }
        public virtual string UrlImagem { get; set; }
        public virtual short Ordem { get; set; }
        public virtual Produto Produto { get; set; }

        public ImagemProduto()
        {
            Ordem = 1;
        }
    }
}
