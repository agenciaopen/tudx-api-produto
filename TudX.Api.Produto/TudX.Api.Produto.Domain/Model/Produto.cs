﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Core.Model;

namespace TudX.Api.Produto.Domain.Model
{
    public class Produto : IEntity
    {
        public virtual long Identificador { get; set; }
        public virtual string Nome { get; set; }
        public virtual string Descricao { get; set; }
        public virtual decimal MenorPreco { get; set; }
        public virtual decimal MaiorPreco { get; set; }
        public virtual long IdentificadorCategoria { get; set; }
        public virtual ICollection<CaracteristicaProduto> Caracteristicas { get; set; }
        public virtual ICollection<ImagemProduto> Imagens { get; set; }
        public virtual ICollection<Anuncio> Anuncios { get; set; }

        public Produto()
        {

        }
    }
}
