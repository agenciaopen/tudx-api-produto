﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Core.Model;

namespace TudX.Api.Produto.Domain.Model
{
    public class Anuncio : IEntity
    {
        public virtual long Identificador { get; set; }
        public virtual string UrlAnuncio { get; set; }
        public virtual string UrlImagem { get; set; }
        public virtual decimal Valor { get; set; }
        public virtual string FormaPagamento { get; set; }
        public virtual bool Disponivel { get; set; }
        public virtual Produto Produto { get; set; }
        public virtual long IdentificadorLoja { get; set; }

        public Anuncio()
        {

        }
    }
}
