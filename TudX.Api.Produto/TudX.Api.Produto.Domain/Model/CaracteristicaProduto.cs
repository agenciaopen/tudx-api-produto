﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Core.Model;

namespace TudX.Api.Produto.Domain.Model
{
    public class CaracteristicaProduto : IEntity
    {
        public virtual long Identificador { get; set; }
        public virtual string Nome { get; set; }
        public virtual string Valor { get; set; }
        public virtual long IdentificadorCategoria { get; set; }
        public virtual Produto Produto { get; set; }

        public CaracteristicaProduto()
        {

        }
    }
}
