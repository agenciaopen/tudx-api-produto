﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TudX.Api.Produto.Infra.Refit.Configuration
{
    public class RefitConfiguration
    {
        public string UrlBaseApiProduto { get; set; }
    }
}
