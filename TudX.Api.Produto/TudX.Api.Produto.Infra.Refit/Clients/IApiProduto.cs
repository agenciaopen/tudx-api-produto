﻿using Refit;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TudX.Api.Produto.Infra.Refit.Clients
{
    public interface IApiProduto
    {
        [Get("/healthz")]
        Task<string> HealthChecksAsync();
    }
}
