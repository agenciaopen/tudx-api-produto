﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Text;
using TudX.IntegrationEvents.Application.Model;

namespace TudX.Api.Produto.Infra.NH.Mappings
{
    public class EventoRecebidoMap : ClassMap<EventoRecebido>
    {
        public EventoRecebidoMap()
        {
            Id(e => e.Identificador).GeneratedBy.Identity();
            Map(e => e.IdentificadorEvento);
            Map(e => e.Nome);
            Map(e => e.Mensagem);
            Map(e => e.DataRecebimento);
        }
    }
}
