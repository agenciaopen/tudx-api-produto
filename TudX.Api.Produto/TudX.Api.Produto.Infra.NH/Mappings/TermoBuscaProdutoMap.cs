﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Produto.Domain.Model;

namespace TudX.Api.Produto.Infra.NH.Mappings
{
    public class TermoBuscaProdutoMap : ClassMap<TermoBuscaProduto>
    {
        public TermoBuscaProdutoMap()
        {
            Id(t => t.Identificador).GeneratedBy.Identity();
            Map(t => t.Nome);
            Map(t => t.DataBusca);
        }
    }
}
