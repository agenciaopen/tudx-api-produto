﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Produto.Domain.Model;

namespace TudX.Api.Produto.Infra.NH.Mappings
{
    public class LojaMap : ClassMap<Loja>
    {
        public LojaMap()
        {
            Id(l => l.Identificador).GeneratedBy.Identity();
            Map(l => l.Nome);
            Map(l => l.UrlLoja);
            Map(l => l.UrlLogo);
        }
    }
}
