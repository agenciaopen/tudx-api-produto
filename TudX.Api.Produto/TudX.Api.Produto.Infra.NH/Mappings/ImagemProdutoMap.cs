﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Produto.Domain.Model;

namespace TudX.Api.Produto.Infra.NH.Mappings
{
    public class ImagemProdutoMap : ClassMap<ImagemProduto>
    {
        public ImagemProdutoMap()
        {
            Id(i => i.Identificador).GeneratedBy.Identity();
            Map(i => i.UrlImagem);
            Map(i => i.Ordem);
            References(c => c.Produto).Column("IdentificadorProduto");
        }
    }
}
