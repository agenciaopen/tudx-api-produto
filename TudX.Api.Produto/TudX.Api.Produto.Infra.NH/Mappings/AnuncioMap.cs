﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Produto.Domain.Model;

namespace TudX.Api.Produto.Infra.NH.Mappings
{
    public class AnuncioMap : ClassMap<Anuncio>
    {
        public AnuncioMap()
        {
            Id(c => c.Identificador).GeneratedBy.Identity();
            Map(p => p.Disponivel);
            Map(p => p.FormaPagamento);
            Map(p => p.UrlAnuncio);
            Map(p => p.UrlImagem);
            Map(p => p.Valor);
            Map(p => p.IdentificadorLoja);
            References(p => p.Produto).Column("IdentificadorProduto");
        }
    }
}
