﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Produto.Domain.Model;

namespace TudX.Api.Produto.Infra.NH.Mappings
{
    public class ProdutoMap : ClassMap<Domain.Model.Produto>
    {
        public ProdutoMap()
        {
            Id(p => p.Identificador).GeneratedBy.Identity();
            Map(p => p.Nome);
            Map(p => p.Descricao).Length(100000);
            Map(p => p.MenorPreco);
            Map(p => p.MaiorPreco);
            Map(p => p.IdentificadorCategoria);

            HasMany(p => p.Anuncios).KeyColumn("IdentificadorProduto").Inverse().Cascade.AllDeleteOrphan();
            HasMany(p => p.Caracteristicas).KeyColumn("IdentificadorProduto").Inverse().Cascade.AllDeleteOrphan();
            HasMany(p => p.Imagens).KeyColumn("IdentificadorProduto").Inverse().Cascade.AllDeleteOrphan();
        }
    }
}
