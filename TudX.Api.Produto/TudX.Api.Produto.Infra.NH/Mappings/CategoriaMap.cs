﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Produto.Domain.Model;

namespace TudX.Api.Produto.Infra.NH.Mappings
{
    public class CategoriaMap : ClassMap<Categoria>
    {
        public CategoriaMap()
        {
            Id(c => c.Identificador).GeneratedBy.Assigned();
            Map(c => c.Nome);
            Map(c => c.Url);
            Map(c => c.IdentificadorSuperior);            
            HasMany(x => x.CategoriasFilhas).Cascade.SaveUpdate().KeyColumn("IdentificadorSuperior");
        }
    }
}
