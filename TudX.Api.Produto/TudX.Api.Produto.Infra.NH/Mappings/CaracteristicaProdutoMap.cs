﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Produto.Domain.Model;

namespace TudX.Api.Produto.Infra.NH.Mappings
{
    public class CaracteristicaProdutoMap : ClassMap<CaracteristicaProduto>
    {
        public CaracteristicaProdutoMap()
        {
            Id(c => c.Identificador).GeneratedBy.Identity();
            Map(c => c.Nome);
            Map(c => c.Valor).Nullable();
            Map(c => c.IdentificadorCategoria);
            References(c => c.Produto).Column("IdentificadorProduto");
        }
    }
}
