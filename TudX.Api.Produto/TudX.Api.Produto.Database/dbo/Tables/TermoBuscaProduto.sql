﻿CREATE TABLE [dbo].[TermoBuscaProduto] (
    [Identificador] BIGINT        IDENTITY (1, 1) NOT NULL,
    [Nome]          VARCHAR (255) NOT NULL,
    [DataBusca]     DATETIME      NOT NULL,
    CONSTRAINT [PK_TermoBuscaProduto] PRIMARY KEY CLUSTERED ([Identificador] ASC)
);

