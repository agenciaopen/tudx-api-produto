﻿CREATE TABLE [dbo].[Produto] (
    [Identificador]          BIGINT          IDENTITY (1, 1) NOT NULL,
    [Nome]                   VARCHAR (255)   NOT NULL,
    [Descricao]              TEXT            NOT NULL,
    [MenorPreco]             DECIMAL (18, 2) NOT NULL,
    [MaiorPreco]             DECIMAL (18, 2) NOT NULL,
    [IdentificadorCategoria] BIGINT          NOT NULL,
    CONSTRAINT [PK_Produto] PRIMARY KEY CLUSTERED ([Identificador] ASC),
    CONSTRAINT [FK_Produto_Categoria] FOREIGN KEY ([IdentificadorCategoria]) REFERENCES [dbo].[Categoria] ([Identificador])
);

