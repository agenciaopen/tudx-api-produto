﻿CREATE TABLE [dbo].[CaracteristicaProduto] (
    [Identificador]          BIGINT        IDENTITY (1, 1) NOT NULL,
    [Nome]                   VARCHAR (50)  NOT NULL,
    [Valor]                  VARCHAR (255) NULL,
    [IdentificadorCategoria] BIGINT        NOT NULL,
    [IdentificadorSuperior]  BIGINT        NULL,
    [IdentificadorProduto]   BIGINT        NULL,
    CONSTRAINT [PK_CaracteristicaProduto] PRIMARY KEY CLUSTERED ([Identificador] ASC),
    CONSTRAINT [FK_CaracteristicaProduto_CaracteristicaProduto] FOREIGN KEY ([IdentificadorSuperior]) REFERENCES [dbo].[CaracteristicaProduto] ([Identificador]),
    CONSTRAINT [FK_CaracteristicaProduto_Categoria] FOREIGN KEY ([IdentificadorCategoria]) REFERENCES [dbo].[Categoria] ([Identificador]),
    CONSTRAINT [FK_CaracteristicaProduto_Produto] FOREIGN KEY ([IdentificadorProduto]) REFERENCES [dbo].[Produto] ([Identificador])
);

