﻿CREATE TABLE [dbo].[Anuncio] (
    [Identificador]        BIGINT          IDENTITY (1, 1) NOT NULL,
    [UrlAnuncio]           VARCHAR (MAX)   NOT NULL,
    [UrlImagem]            VARCHAR (MAX)   NOT NULL,
    [Valor]                DECIMAL (18, 2) NOT NULL,
    [FormaPagamento]       VARCHAR (255)   NULL,
    [Disponivel]           BIT             NOT NULL,
    [IdentificadorProduto] BIGINT          NOT NULL,
    [IdentificadorLoja]    BIGINT          NULL,
    CONSTRAINT [PK_Anuncio] PRIMARY KEY CLUSTERED ([Identificador] ASC),
    CONSTRAINT [FK_Anuncio_Loja] FOREIGN KEY ([IdentificadorLoja]) REFERENCES [dbo].[Loja] ([Identificador]),
    CONSTRAINT [FK_Anuncio_Produto] FOREIGN KEY ([IdentificadorProduto]) REFERENCES [dbo].[Produto] ([Identificador])
);

