﻿CREATE TABLE [dbo].[ImagemProduto] (
    [Identificador]        BIGINT        IDENTITY (1, 1) NOT NULL,
    [UrlImagem]            VARCHAR (MAX) NOT NULL,
    [Ordem]                SMALLINT      NOT NULL,
    [IdentificadorProduto] BIGINT        NOT NULL,
    CONSTRAINT [PK_ImagemProduto] PRIMARY KEY CLUSTERED ([Identificador] ASC),
    CONSTRAINT [FK_ImagemProduto_Produto] FOREIGN KEY ([IdentificadorProduto]) REFERENCES [dbo].[Produto] ([Identificador])
);



