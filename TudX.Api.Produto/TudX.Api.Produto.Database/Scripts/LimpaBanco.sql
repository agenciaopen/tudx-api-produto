﻿USE TudXProduto

DELETE FROM dbo.CaracteristicaProduto
DBCC CHECKIDENT ('dbo.CaracteristicaProduto', RESEED, 0);
DELETE FROM dbo.ImagemProduto
DBCC CHECKIDENT ('dbo.ImagemProduto', RESEED, 0);
DELETE FROM dbo.Anuncio
DBCC CHECKIDENT ('dbo.Anuncio', RESEED, 0);
DELETE FROM dbo.Produto
DBCC CHECKIDENT ('dbo.Produto', RESEED, 0);
DELETE FROM dbo.Categoria;
DELETE FROM dbo.Loja;
