﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TudX.Api.Produto.Infra.Refit.Clients;

namespace TudX.Api.Produto.BackgroundTasks.Job
{
    public class HealthChecksJob : IJob
    {
        private readonly ILogger<HealthChecksJob> _logger;
        private readonly IServiceProvider serviceProvider;

        public HealthChecksJob(IServiceProvider serviceProvider, ILogger<HealthChecksJob> logger)
        {
            _logger = logger;
            this.serviceProvider = serviceProvider;
        }

        public Task Execute(IJobExecutionContext context)
        {
            using (var scope = serviceProvider.CreateScope())
            {
                // Resolve the Scoped service
                var apiCrawler = scope.ServiceProvider.GetService<IApiProduto>();
                string teste = apiCrawler.HealthChecksAsync().Result;
                _logger.LogInformation(teste);
            }

            return Task.CompletedTask;
        }
    }
}
