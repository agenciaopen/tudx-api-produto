﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TudX.Api.Produto.Application.Poco
{
    public class FiltroCaracteristicaPOCO
    {
        public string Nome { get; set; }
        public string Valores { get; set; }
    }
}
