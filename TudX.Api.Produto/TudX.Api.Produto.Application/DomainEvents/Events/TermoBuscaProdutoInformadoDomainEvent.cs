﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace TudX.Api.Produto.Application.DomainEvents.Events
{
    public class TermoBuscaProdutoInformadoDomainEvent : INotification
    {
        public string TermoPesquisado { get; set; }
    }
}
