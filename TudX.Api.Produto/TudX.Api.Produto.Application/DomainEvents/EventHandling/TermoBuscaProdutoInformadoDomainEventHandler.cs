﻿using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TudX.Api.Produto.Application.DomainEvents.Events;
using TudX.Api.Produto.Application.Services;
using TudX.IntegrationEvents.EventBus.Abstractions;

namespace TudX.Api.Produto.Application.DomainEvents.EventHandling
{
    public class TermoBuscaProdutoInformadoDomainEventHandler : INotificationHandler<TermoBuscaProdutoInformadoDomainEvent>
    {
        private readonly TermoBuscaProdutoService termoBuscaProdutoService;
        private readonly ILogger _logger;
        private readonly IEventBus _eventBus;

        public TermoBuscaProdutoInformadoDomainEventHandler(IServiceProvider serviceProvider,
                                                            ILoggerFactory loggerFactory,
                                                            IEventBus eventBus)
        {
            termoBuscaProdutoService = new TermoBuscaProdutoService(serviceProvider);
            _logger = loggerFactory?.CreateLogger<TermoBuscaProdutoInformadoDomainEventHandler>() ?? throw new ArgumentNullException(nameof(loggerFactory));
            _eventBus = eventBus;
        }

        public async Task Handle(TermoBuscaProdutoInformadoDomainEvent notification, CancellationToken cancellationToken)
        {
            try
            {
                termoBuscaProdutoService.TratarTermoInformado(notification.TermoPesquisado);
            }
            catch (Exception ex)
            {
                _logger.LogError(string.Format("Ocorreu um erro ao processar a mensagem: {0}", ex.Message));
            }
        }
    }
}
