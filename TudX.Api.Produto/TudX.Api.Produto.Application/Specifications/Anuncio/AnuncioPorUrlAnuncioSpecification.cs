﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TudX.Api.Produto.Domain.Model;
using TudX.Specification;

namespace TudX.Api.Produto.Application.Specifications
{
    public class AnuncioPorUrlAnuncioSpecification : SpecificationBase<Anuncio>
    {
        private string urlAnuncio;

        public AnuncioPorUrlAnuncioSpecification(string urlAnuncio)
        {
            this.urlAnuncio = urlAnuncio;
        }

        public override Expression<Func<Anuncio, bool>> SatisfiedBy()
        {
            return a => a.UrlAnuncio.Trim().ToUpper() == urlAnuncio.Trim().ToUpper();
        }
    }
}
