﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TudX.Api.Produto.Domain.Model;
using TudX.Specification;

namespace TudX.Api.Produto.Application.Specifications
{
    public class TermoBuscaProdutoPorNomeEDataBuscaSpecification : SpecificationBase<TermoBuscaProduto>
    {
        private string nome;
        private DateTime dataBusca;

        public TermoBuscaProdutoPorNomeEDataBuscaSpecification(string nome, DateTime dataBusca)
        {
            this.nome = nome;
            this.dataBusca = dataBusca;
        }

        public override Expression<Func<TermoBuscaProduto, bool>> SatisfiedBy()
        {
            return t => t.Nome.ToUpper().Trim() == nome.ToUpper().Trim() && t.DataBusca >= dataBusca;
        }
    }
}
