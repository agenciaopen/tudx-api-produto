﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TudX.Api.Produto.Domain.Model;
using TudX.Specification;

namespace TudX.Api.Produto.Application.Specifications
{
    public class TermoBuscaProdutoPorNomeIniciadoEmEDataBuscaSpecification : SpecificationBase<TermoBuscaProduto>
    {
        private string nome;
        private DateTime dataBusca;

        public TermoBuscaProdutoPorNomeIniciadoEmEDataBuscaSpecification(string nome, DateTime dataBusca)
        {
            this.nome = nome;
            this.dataBusca = dataBusca;
        }

        public override Expression<Func<TermoBuscaProduto, bool>> SatisfiedBy()
        {
            return t => t.Nome.ToUpper().Trim().StartsWith(nome.ToUpper().Trim()) && t.DataBusca >= dataBusca;
        }
    }
}
