﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TudX.Api.Produto.Domain.Model;
using TudX.Specification;

namespace TudX.Api.Produto.Application.Specifications
{
    public class ProdutoSimilarPorPalavrasSpecification : SpecificationBase<Domain.Model.Produto>
    {
        private List<string> palavras;

        public ProdutoSimilarPorPalavrasSpecification(List<string> palavras)
        {
            this.palavras = palavras;
        }

        public override Expression<Func<Domain.Model.Produto, bool>> SatisfiedBy()
        {
            foreach (var palavra in palavras)
            {
                AddExpression(p => p.Nome.ToUpper().Trim().Contains(palavra.ToUpper().Trim()));
            }

            return GetExpression();
        }
    }
}
