﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using TudX.Api.Produto.Application.Poco;
using TudX.Api.Produto.Domain.Model;
using TudX.Specification;

namespace TudX.Api.Produto.Application.Specifications
{
    public class ProdutoParaListagemSpecification : SpecificationBase<Domain.Model.Produto>
    {
        private string nome;
        private List<FiltroCaracteristicaPOCO> filtrosCaracteristica;

        public ProdutoParaListagemSpecification(string nome)
        {
            this.nome = nome;
        }

        public ProdutoParaListagemSpecification(string nome, List<FiltroCaracteristicaPOCO> filtrosCaracteristica)
        {
            this.nome = nome;
            this.filtrosCaracteristica = filtrosCaracteristica;
        }

        public override Expression<Func<Domain.Model.Produto, bool>> SatisfiedBy()
        {
            AddExpression(p => p.Anuncios.Any(a => a.Disponivel));
            if (!string.IsNullOrEmpty(nome))
            {
                List<Expression<Func<Domain.Model.Produto, bool>>> expressoesPossiveisDoNome = new List<Expression<Func<Domain.Model.Produto, bool>>>();                
                List<string> partesNome = nome.Split(' ').ToList();
                if (partesNome.Count > 1)
                {
                    expressoesPossiveisDoNome.Add(p => p.Nome.ToUpper().Contains(nome.ToUpper()));
                    foreach (var parte in partesNome)
                    {
                        expressoesPossiveisDoNome.Add(p => p.Nome.ToUpper().Contains(parte.ToUpper()));
                    }
                    AddExpressionOr(expressoesPossiveisDoNome);
                } else
                {
                    AddExpression(p => p.Nome.ToUpper().Contains(nome.ToUpper()));
                }
            }

            if (filtrosCaracteristica != null)
            {
                foreach (var filtroCaracteristica in filtrosCaracteristica)
                {
                    AddExpression(p => p.Caracteristicas.Any(c => c.Nome == filtroCaracteristica.Nome
                                                            && filtroCaracteristica.Valores.ToUpper().Trim().Contains(c.Valor.ToUpper().Trim())));
                }
            }

            return GetExpression();
        }
    }
}
