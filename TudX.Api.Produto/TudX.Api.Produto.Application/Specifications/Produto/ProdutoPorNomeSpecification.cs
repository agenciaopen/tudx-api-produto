﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TudX.Api.Produto.Domain.Model;
using TudX.Specification;

namespace TudX.Api.Produto.Application.Specifications
{
    public class ProdutoPorNomeSpecification : SpecificationBase<Domain.Model.Produto>
    {
        private string Nome { get; set; }

        public ProdutoPorNomeSpecification(string nome)
        {
            Nome = nome;
        }

        public override Expression<Func<Domain.Model.Produto, bool>> SatisfiedBy()
        {
            return p => p.Nome.ToUpper() == Nome.ToUpper();
        }
    }
}
