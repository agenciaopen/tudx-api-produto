﻿using Mapster;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TudX.Adapter.MediatR;
using TudX.Api.Produto.Application.DomainEvents.Events;
using TudX.Api.Produto.Application.Poco;
using TudX.Api.Produto.Application.Specifications;
using TudX.Api.Produto.CrossCuting.Data;
using TudX.Api.Produto.Domain.Model;
using TudX.Core;
using TudX.Core.Pagination;
using TudX.Domain.Base;
using TudX.Domain.Base.Repository;
using TudX.Exceptions;
using TudX.Specification.Business;

namespace TudX.Api.Produto.Application.Services
{
    public class ProdutoService : DomainServiceRelationalMediatRBase<Domain.Model.Produto>
    {
        private readonly CategoriaService categoriaService;
        private readonly AnuncioService anuncioService;
        private readonly LojaService lojaService;
        private readonly TermoBuscaProdutoService termoBuscaProdutoService;

        public ProdutoService(IServiceProvider serviceProvider, IMediator mediator) : base(serviceProvider, mediator)
        {
            categoriaService = new CategoriaService(serviceProvider);
            anuncioService = new AnuncioService(serviceProvider);
            lojaService = new LojaService(serviceProvider);
            termoBuscaProdutoService = new TermoBuscaProdutoService(serviceProvider);
        }

        public async Task<bool> ReceberProdutoCategorizado(ProdutoCategorizadoData produtoCategorizadoData)
        {
            bool isUpdate = true;

            Domain.Model.Produto produto = this.FindSingleBySpecification(new ProdutoPorNomeSpecification(produtoCategorizadoData.Nome));
            if (produto == null)
            {
                isUpdate = false;
                produto = new Domain.Model.Produto();                
            }

            AtribuirInformacoesDadosBasicoProduto(produtoCategorizadoData, isUpdate, produto);
            AtribuirInformacoesProduto(produto, produtoCategorizadoData);

            if (produto.Imagens == null || produto.Imagens.Count <= 0)
                return false;

            this.Save(produto);
            this.SaveChanges();

            return true;
        }

        public BaseResultPagination<ProdutoListagemData, ProdutoFiltroData> BuscarProdutosParaListagem(BaseFilterPagination<ProdutoFiltroData> filtro)
        {
            int total = 0;
            ProdutoParaListagemSpecification spec = null;
            if (filtro.Filter.FiltrosCaracteristica == null)
            {
                spec = new ProdutoParaListagemSpecification(filtro.Filter.Nome);
            }
            else
            {
                List<FiltroCaracteristicaPOCO> filtrosCaracteristicaPOCO = CriarFiltrosCaracteristicaPOCO(filtro.Filter.FiltrosCaracteristica);
                spec = new ProdutoParaListagemSpecification(filtro.Filter.Nome, filtrosCaracteristicaPOCO);
            }

            spec.Pagination = new BasePagination(filtro.CurrentPage, filtro.Take);
            List<Domain.Model.Produto> produtos = this.FindBySpecification(spec, out total).ToList();
            List<ProdutoListagemData> produtosData = new List<ProdutoListagemData>();

            foreach (var produto in produtos)
            {
                Anuncio primeiroAnuncio = produto.Anuncios.FirstOrDefault();

                ProdutoListagemData produtoVO = produto.Adapt<ProdutoListagemData>();
                produtoVO.FormaPagamento = primeiroAnuncio.FormaPagamento;
                produtoVO.Url = primeiroAnuncio.UrlImagem;
                produtoVO.QuantidadeAnuncios = produto.Anuncios.Count(a => a.Disponivel);

                produtosData.Add(produtoVO);
            }

            BaseResultPagination<ProdutoListagemData, ProdutoFiltroData> retorno
                = new BaseResultPagination<ProdutoListagemData, ProdutoFiltroData>(produtosData.Adapt<List<ProdutoListagemData>>());

            retorno = filtro.Adapt(retorno);
            retorno.TotalItems = total;

            RegistrarTermoInformadoNaBuscaDeProdutos(filtro);

            return retorno;
        }

        private void RegistrarTermoInformadoNaBuscaDeProdutos(BaseFilterPagination<ProdutoFiltroData> filtro)
        {
            TermoBuscaProdutoInformadoDomainEvent termoInformadoNotification = new TermoBuscaProdutoInformadoDomainEvent();
            termoInformadoNotification.TermoPesquisado = filtro.Filter.Nome;
            this.AddDomainEvent(termoInformadoNotification);
            this.DispatchDomainEventsAsync();
        }

        private List<FiltroCaracteristicaPOCO> CriarFiltrosCaracteristicaPOCO(List<FiltroData> filtrosCaracteristica)
        {
            List<FiltroCaracteristicaPOCO> filtros = new List<FiltroCaracteristicaPOCO>();

            foreach (var filtroCaracteristica in filtrosCaracteristica)
            {
                FiltroCaracteristicaPOCO filtroCaracteristicaPOCO = new FiltroCaracteristicaPOCO();
                filtroCaracteristicaPOCO.Nome = filtroCaracteristica.Nome;
                if (filtroCaracteristica.Opcoes == null || filtroCaracteristica.Opcoes.Count <= 0)
                    continue;

                filtroCaracteristicaPOCO.Valores = String.Join("", filtroCaracteristica.Opcoes.Select(o => o.Valor));
                filtros.Add(filtroCaracteristicaPOCO);
            }

            return filtros;
        }

        public async Task<ProdutoDetalheData> BuscarProdutoPorIdentificador(long identificador)
        {
            Domain.Model.Produto produto = this.FindById(identificador);

            if (produto == null)
                throw new BusinessException("Produto não encontrado");

            Categoria categoria = categoriaService.FindById(produto.IdentificadorCategoria);
            CategoriaDetalheData categoriaDetalheData = GerarMigalhaDePao(categoria, categoria.Adapt<CategoriaDetalheData>());
            ProdutoDetalheData produtoDetalheData = produto.Adapt<ProdutoDetalheData>();
            foreach (var item in produtoDetalheData.Imagens)
            {
                item.UrlImagem = item.UrlImagem;
            }
            produtoDetalheData.Categoria = categoriaDetalheData;
            AtribuirUrlLogoDaLojaDoAnuncio(produtoDetalheData, produto);

            return produtoDetalheData;
        }

        public List<string> BuscarTermosDeProdutosJaConsultados(string nome)
        {
            TermoBuscaProdutoPorNomeIniciadoEmEDataBuscaSpecification spec = new TermoBuscaProdutoPorNomeIniciadoEmEDataBuscaSpecification(nome, DateTime.Now.AddMonths(-3));
            spec.Pagination.Take = 10;
            spec.AddOrderBy("DataBusca", Core.Pagination.Enum.SortDirection.desc);
            List<TermoBuscaProduto> termoBuscaProdutos = termoBuscaProdutoService.FindBySpecification(spec).ToList();

            if (termoBuscaProdutos == null)
                return new List<string>();

            return termoBuscaProdutos.Select(t => t.Nome).ToList();
        }

        #region Métodos privados
        private void AtribuirInformacoesDadosBasicoProduto(ProdutoCategorizadoData produtoCategorizadoData, bool isUpdate, Domain.Model.Produto produto)
        {
            if (!isUpdate)
            {
                produto.Nome = produtoCategorizadoData.Nome;
                produto.Descricao = produtoCategorizadoData.Descricao;
                produto.MenorPreco = produtoCategorizadoData.Valor;
                produto.MaiorPreco = produtoCategorizadoData.Valor;
            }
            else
            {
                if (produto.MenorPreco > produtoCategorizadoData.Valor)
                    produto.MenorPreco = produtoCategorizadoData.Valor;

                if (produto.MaiorPreco < produtoCategorizadoData.Valor)
                    produto.MaiorPreco = produtoCategorizadoData.Valor;
            }

            produto.IdentificadorCategoria = produtoCategorizadoData.IdentificadorCategoria;
        }

        private void AtribuirInformacoesProduto(Domain.Model.Produto produto, ProdutoCategorizadoData produtoCategorizadoData)
        {
            AtribuirInformacoesAnuncio(produto, produtoCategorizadoData);
            AtrubuirInformacoesCaracteristicas(produto, produtoCategorizadoData);
            AtrubuirInformacoesImagens(produto, produtoCategorizadoData);
        }

        private void AtrubuirInformacoesImagens(Domain.Model.Produto produto, ProdutoCategorizadoData produtoCategorizadoData)
        {
            if (produtoCategorizadoData.Imagens == null)
                return;

            if (produto.Imagens == null)
                produto.Imagens = new List<ImagemProduto>();

            foreach (var imagemCategorizado in produtoCategorizadoData.Imagens)
            {
                ImagemProduto imagemProduto = produto.Imagens.FirstOrDefault(c => c.UrlImagem.ToUpper() == imagemCategorizado.UrlImagem.ToUpper());
                if (imagemProduto == null)
                {
                    imagemProduto = new ImagemProduto();
                    imagemProduto.UrlImagem = imagemCategorizado.UrlImagem;
                    if (produto.Imagens.Count > 0)
                        imagemProduto.Ordem = Convert.ToInt16(produto.Imagens.Max(i => i.Ordem) + 1);                    

                    imagemProduto.Produto = produto;
                    produto.Imagens.Add(imagemProduto);
                }
            }
        }

        private void AtrubuirInformacoesCaracteristicas(Domain.Model.Produto produto, ProdutoCategorizadoData produtoCategorizadoData)
        {
            if (produtoCategorizadoData.Caracteristicas == null)
                return;

            if (produto.Caracteristicas == null)
                produto.Caracteristicas = new List<CaracteristicaProduto>();

            foreach (var caracteristicaCategorizado in produtoCategorizadoData.Caracteristicas)
            {
                CaracteristicaProduto caracteristicaProduto = produto.Caracteristicas.FirstOrDefault(c => c.Nome.ToUpper() == caracteristicaCategorizado.Nome.ToUpper());
                if (caracteristicaProduto == null)
                {
                    caracteristicaProduto = new CaracteristicaProduto();
                    caracteristicaProduto.Nome = caracteristicaCategorizado.Nome;
                    caracteristicaProduto.Valor = caracteristicaCategorizado.Valor;
                    caracteristicaProduto.Produto = produto;
                    caracteristicaProduto.IdentificadorCategoria = produtoCategorizadoData.IdentificadorCategoria;
                    produto.Caracteristicas.Add(caracteristicaProduto);
                }
                else
                {
                    caracteristicaProduto.Valor = caracteristicaCategorizado.Valor;
                }
            }
        }

        private void AtribuirInformacoesAnuncio(Domain.Model.Produto produto, ProdutoCategorizadoData produtoCategorizadoData)
        {
            if (produto.Anuncios == null)
                produto.Anuncios = new List<Anuncio>();

            bool isUpdate = true;
            Anuncio anuncio = produto.Anuncios.FirstOrDefault(a => a.UrlAnuncio.ToUpper().Trim() == produtoCategorizadoData.UrlAnuncio.ToUpper().Trim());
            if (anuncio == null)
            {
                anuncio = new Anuncio();
                isUpdate = false;
                anuncio.IdentificadorLoja = produtoCategorizadoData.IdentificadorLoja;
                anuncio.UrlAnuncio = produtoCategorizadoData.UrlAnuncio;
            }

            anuncio.Disponivel = true;
            anuncio.FormaPagamento = produtoCategorizadoData.FormaPagamento;
            if (produtoCategorizadoData.Imagens != null && produtoCategorizadoData.Imagens.Count > 0)
                anuncio.UrlImagem = produtoCategorizadoData.Imagens.ToList()[0].UrlImagem;
            anuncio.Valor = produtoCategorizadoData.Valor;

            if (!isUpdate)
            {
                anuncio.Produto = produto;
                produto.Anuncios.Add(anuncio);
            }
        }

        private void AtribuirUrlLogoDaLojaDoAnuncio(ProdutoDetalheData produtoDetalheData, Domain.Model.Produto produto)
        {
            foreach (var anuncio in produtoDetalheData.Anuncios)
            {
                long identificadorLoja = produto.Anuncios.FirstOrDefault(a => a.Identificador == anuncio.Identificador).IdentificadorLoja;
                anuncio.UrlLogoLoja = lojaService.FindById(identificadorLoja).UrlLogo;
            }
        }

        private CategoriaDetalheData GerarMigalhaDePao(Categoria categoria, CategoriaDetalheData categoriaDetalheData)
        {
            if (categoria.IdentificadorSuperior.HasValue)
            {
                Categoria categoriaSuperior = categoriaService.FindById(categoria.IdentificadorSuperior.Value);
                CategoriaDetalheData categoriaDetalheVOProcess = categoriaSuperior.Adapt<CategoriaDetalheData>();
                categoriaDetalheVOProcess.CategoriaFilha = categoriaDetalheData;
                categoriaDetalheData = categoriaDetalheVOProcess;
                categoriaDetalheData = GerarMigalhaDePao(categoriaSuperior, categoriaDetalheData);
            } 

            return categoriaDetalheData;
        }
        #endregion
    }
}
