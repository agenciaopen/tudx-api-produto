﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Produto.Application.Specifications;
using TudX.Api.Produto.Domain.Model;
using TudX.Domain.Base;

namespace TudX.Api.Produto.Application.Services
{
    public class TermoBuscaProdutoService : DomainServiceRelationalBase<TermoBuscaProduto>
    {
        public TermoBuscaProdutoService(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }

        public void TratarTermoInformado(string termo)
        {
            DateTime dataBusca = DateTime.Now.AddMonths(-3);
            TermoBuscaProdutoPorNomeEDataBuscaSpecification spec = new TermoBuscaProdutoPorNomeEDataBuscaSpecification(termo, dataBusca);
            TermoBuscaProduto termoBuscaProduto = this.FindSingleBySpecification(spec);
            if (termoBuscaProduto != null)
                return;

            termoBuscaProduto = new TermoBuscaProduto();
            termoBuscaProduto.Nome = termo;
            termoBuscaProduto.DataBusca = DateTime.Now;

            this.Save(termoBuscaProduto);
        }
    }
}
