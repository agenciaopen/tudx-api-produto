﻿using Mapster;
using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Produto.CrossCuting.Data;
using TudX.Api.Produto.Domain.Model;
using TudX.Domain.Base;
using TudX.Domain.Base.Repository;
using TudX.Exceptions;

namespace TudX.Api.Produto.Application.Services
{
    public class CategoriaService : DomainServiceRelationalBase<Categoria>
    {
        public CategoriaService(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }

        public Categoria InserirCargaCategoria(CargaCategoriaIntegrationEventData categoriaintegrationEventData)
        {
            Categoria categoria = categoriaintegrationEventData.Adapt<Categoria>();

            this.Create(categoria);
            this.SaveChanges();

            return categoria;
        }

        public Categoria AtualizarCargaCategoria(CargaCategoriaIntegrationEventData categoriaintegrationEventData)
        {
            Categoria categoria = categoriaintegrationEventData.Adapt<Categoria>();

            this.Update(categoria);
            this.SaveChanges();

            return categoria;
        }

        public Categoria InserirCategoria(CategoriaIntegrationEventData categoriaintegrationEventData)
        {
            Categoria categoria = categoriaintegrationEventData.Adapt<Categoria>();

            this.Create(categoria);
            this.SaveChanges();

            return categoria;
        }

        public Categoria AtualizarCategoria(CategoriaIntegrationEventData categoriaintegrationEventData)
        {
            Categoria categoria = this.FindById(categoriaintegrationEventData.Identificador);
            if (categoria == null)
                throw new BusinessException("Não foi encontrada a categoria para atualização.");

            categoria.Nome = categoriaintegrationEventData.Nome;
            categoria.IdentificadorSuperior = categoriaintegrationEventData.IdentificadorSuperior;

            this.Update(categoria);
            this.SaveChanges();

            return categoria;
        }
    }
}
