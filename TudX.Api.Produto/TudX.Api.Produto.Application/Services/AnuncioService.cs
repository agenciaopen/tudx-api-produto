﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Produto.Domain.Model;
using TudX.Domain.Base;

namespace TudX.Api.Produto.Application.Services
{
    public class AnuncioService : DomainServiceRelationalBase<Anuncio>
    {
        public AnuncioService(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }
    }
}
