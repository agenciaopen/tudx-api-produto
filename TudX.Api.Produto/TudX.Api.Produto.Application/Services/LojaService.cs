﻿using Mapster;
using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Produto.CrossCuting.Data;
using TudX.Api.Produto.Domain.Model;
using TudX.Domain.Base;

namespace TudX.Api.Produto.Application.Services
{
    public class LojaService : DomainServiceRelationalBase<Loja>
    {
        public LojaService(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }

        public Loja InserirLoja(LojaIntegrationEventData lojaData)
        {
            Loja loja = lojaData.Adapt<Loja>();

            this.Create(loja);
            this.SaveChanges();

            return loja;
        }

        public Loja AtualizarLoja(LojaIntegrationEventData lojaData)
        {
            Loja loja = lojaData.Adapt<Loja>();

            this.Create(loja);
            this.SaveChanges();

            return loja;
        }
    }
}
