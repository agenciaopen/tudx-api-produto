﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TudX.Api.Produto.CrossCuting.Data
{
    public class LojaIntegrationEventData
    {
        public long Identificador { get; set; }
        public string Nome { get; set; }
        public string UrlLoja { get; set; }
        public string UrlLogo { get; set; }
    }
}
