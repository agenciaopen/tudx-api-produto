﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TudX.Api.Produto.CrossCuting.Data
{
    public class ImagemDetalheData
    {
        public long Identificador { get; set; }
        public string UrlImagem { get; set; }
        public short Ordem { get; set; }
    }
}
