﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TudX.Api.Produto.CrossCuting.Data
{
    public class CaracteristicaCategorizadoData
    {
        public long Identificador { get; set; }
        public string Nome { get; set; }
        public string Valor { get; set; }
        public long IdentificadorCaracteristica { get; set; }
    }
}
