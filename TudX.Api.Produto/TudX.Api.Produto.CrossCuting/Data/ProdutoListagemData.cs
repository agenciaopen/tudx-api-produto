﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TudX.Api.Produto.CrossCuting.Data
{
    public class ProdutoListagemData
    {
        public long Identificador { get; set; }
        public string Nome { get; set; }
        public decimal MenorPreco { get; set; }
        public string Url { get; set; }
        public string FormaPagamento { get; set; }
        public int QuantidadeAnuncios { get; set; }
    }
}
