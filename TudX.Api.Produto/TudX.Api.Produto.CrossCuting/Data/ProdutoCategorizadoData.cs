﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TudX.Api.Produto.CrossCuting.Data
{
    public class ProdutoCategorizadoData
    {
        public long Identificador { get; set; }
        public string CodigoProduto { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string UrlAnuncio { get; set; }
        public decimal Valor { get; set; }
        public string FormaPagamento { get; set; }
        public long IdentificadorLoja { get; set; }
        public long IdentificadorCategoria { get; set; }
        public long IdentificadorProdutoCrawler { get; set; }
        public ICollection<CaracteristicaCategorizadoData> Caracteristicas { get; set; }
        public ICollection<ImagemCategorizadoData> Imagens { get; set; }
    }
}
