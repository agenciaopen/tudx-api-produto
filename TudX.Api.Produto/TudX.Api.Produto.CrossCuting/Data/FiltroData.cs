﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TudX.Api.Produto.CrossCuting.Data
{
    public class FiltroData
    {
        public string Nome { get; set; }                
        public List<OpcaoFiltroData> Opcoes { get; set; }
    }
}
