﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TudX.Api.Produto.CrossCuting.Data
{
    public class CaracteristicaDetalheData
    {
        public long Identificador { get; set; }
        public string Nome { get; set; }
        public string Valor { get; set; }
    }
}
