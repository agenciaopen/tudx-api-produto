﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TudX.Core.Filters;

namespace TudX.Api.Produto.CrossCuting.Data
{
    public class CategoriaDetalheData
    {
        public long Identificador { get; set; }
        public string Nome { get; set; }
        public string Url { get; set; }
        [SwaggerExclude]
        public CategoriaDetalheData CategoriaFilha { get; set; }
    }
}
