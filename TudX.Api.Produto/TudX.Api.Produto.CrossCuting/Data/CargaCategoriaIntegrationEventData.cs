﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TudX.Api.Produto.CrossCuting.Data
{
    public class CargaCategoriaIntegrationEventData
    {
        public long Identificador { get; set; }
        public string Nome { get; set; }
        public List<CargaCategoriaIntegrationEventData> CategoriasFilhas { get; set; }
    }
}
