﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TudX.Api.Produto.CrossCuting.Data
{
    public class ImagemCategorizadoData
    {
        public long Identificador { get; set; }
        public string UrlImagem { get; set; }
    }
}
