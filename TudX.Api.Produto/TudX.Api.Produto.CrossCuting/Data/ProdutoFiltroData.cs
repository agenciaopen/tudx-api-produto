﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TudX.Api.Produto.CrossCuting.Data
{
    public class ProdutoFiltroData
    {
        public string Nome { get; set; }        
        public List<FiltroData> FiltrosCaracteristica { get; set; }
    }
}
