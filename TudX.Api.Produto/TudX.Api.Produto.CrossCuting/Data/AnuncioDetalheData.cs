﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TudX.Api.Produto.CrossCuting.Data
{
    public class AnuncioDetalheData
    {
        public long Identificador { get; set; }
        public string UrlAnuncio { get; set; }
        public string UrlImagem { get; set; }
        public decimal Valor { get; set; }
        public string FormaPagamento { get; set; }
        public string UrlLogoLoja { get; set; }
    }
}
