﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TudX.Api.Produto.CrossCuting.Data
{
    public class ProdutoDetalheData
    {
        public long Identificador { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public decimal MenorPreco { get; set; }
        public decimal MaiorPreco { get; set; }
        public List<AnuncioDetalheData> Anuncios { get; set; }
        public List<ImagemDetalheData> Imagens { get; set; }
        public List<CaracteristicaDetalheData> Caracteristicas { get; set; }
        public CategoriaDetalheData Categoria { get; set; }
    }
}
