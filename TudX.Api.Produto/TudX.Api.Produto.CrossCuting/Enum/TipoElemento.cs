﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TudX.Api.Produto.CrossCuting.Enum
{
    public enum TipoElemento : short
    {
        Nenhum = 0,
        CheckBox = 1
    }
}
