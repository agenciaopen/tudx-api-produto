﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TudX.Adapter.Eventbus.RabbitMQ.Extensions;
using TudX.Api.Base;
using TudX.Api.Base.Metadata;
using TudX.Api.Produto.Application.DomainEvents.EventHandling;
using TudX.Api.Produto.Infra.NH.DependencyInjection;
using TudX.Api.Produto.IntegrationEvents.EventHandling;
using TudX.Api.Produto.IntegrationEvents.Events;
using TudX.IntegrationEvents.EventBus.Abstractions;

namespace TudX.Api.Produto
{
    /// <summary>
    /// Configuração de inicio da aplicação.
    /// </summary>
    public class Startup : TudxApiBaseStartup
    {
        /// <summary>
        /// Informações a serem apresentadas na documentação do swagger.
        /// </summary>
        protected override ApiMetadata ApiMetadata => new ApiMetadata()
        {
            Name = "Api Produto",
            Description = "Esta api será utilizada para gerenciar os dados de produtos do TudX.",
            DefaultApiVersion = "1.0"
        };

        /// <summary>
        /// Construtor.
        /// </summary>
        /// <param name="configuration">Informações de configurações da aplicação.</param>
        public Startup(IConfiguration configuration) : base(configuration)
        {

        }

        /// <summary>
        /// Métodos de configurações de IoC
        /// </summary>
        /// <param name="services">Classe responsável por configurar o DI.</param>
        protected override void ConfigureApiServices(IServiceCollection services)
        {
            services.AddControllers().AddNewtonsoftJson();
            services.AddCors(options =>
            {
                options.AddPolicy("TudXPolicy",
                builder =>
                {
                    builder.WithOrigins("http://tudx01",
                                        "http://localhost",
                                        "http://localhost:4200")
                                        .AllowAnyHeader()
                                        .AllowAnyMethod()
                                        .AllowCredentials();
                });
            });

            services.AddNHAdapter(Configuration);
            services.AddRabbitMQAdapter(Configuration);
            services.AddMediatR(typeof(TermoBuscaProdutoInformadoDomainEventHandler).Assembly);

            services.AddTransient<ProdutoCategorizadoIntegrationEventHandler>();
            services.AddTransient<CargaCategoriaAdicionadaIntegrationEventHandler>();
            services.AddTransient<CargaCategoriaAlteradaIntegrationEventHandler>();
            services.AddTransient<CategoriaAdicionadaIntegrationEventHandler>();
            services.AddTransient<CategoriaAlteradaIntegrationEventHandler>();
            services.AddTransient<LojaAdicionadaIntegrationEventHandler>();
            services.AddTransient<LojaAlteradaIntegrationEventHandler>();
        }

        /// <summary>
        /// Método de configuração da aplicação.
        /// </summary>
        /// <param name="app">Construtor da aplicação.</param>
        protected override void ConfigureApi(IApplicationBuilder app)
        {
            var eventBus = app.ApplicationServices.GetRequiredService<IEventBus>();

            eventBus.Subscribe<ProdutoCategorizadoIntegrationEvent, ProdutoCategorizadoIntegrationEventHandler>();
            eventBus.Subscribe<CargaCategoriaAdicionadaIntegrationEvent, CargaCategoriaAdicionadaIntegrationEventHandler>();
            eventBus.Subscribe<CargaCategoriaAlteradaIntegrationEvent, CargaCategoriaAlteradaIntegrationEventHandler>();
            eventBus.Subscribe<CategoriaAdicionadaIntegrationEvent, CategoriaAdicionadaIntegrationEventHandler>();
            eventBus.Subscribe<CategoriaAlteradaIntegrationEvent, CategoriaAlteradaIntegrationEventHandler>();
            eventBus.Subscribe<LojaAdicionadaIntegrationEvent, LojaAdicionadaIntegrationEventHandler>();
            eventBus.Subscribe<LojaAlteradaIntegrationEvent, LojaAlteradaIntegrationEventHandler>();

            app.UseCors("TudXPolicy");
            app.UseRouting();
            app.UseEndpoints(endpoints => {
                endpoints.MapControllers();
            });
        }
    }
}

