﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TudX.IntegrationEvents.Events;

namespace TudX.Api.Produto.IntegrationEvents.Events
{
    public class CargaCategoriaAdicionadaIntegrationEvent : IntegrationEvent
    {
        public long Identificador { get; set; }
        public string Nome { get; set; }
        public List<CargaCategoriaAdicionadaIntegrationEvent> CategoriasFilhas { get; set; }
    }
}
