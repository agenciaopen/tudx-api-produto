﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TudX.Api.Produto.IntegrationEvents.Events.Data
{
    public class CaracteristicaCategorizadoDataIntegrationEvent
    {
        public long Identificador { get; set; }
        public string Nome { get; set; }
        public string Valor { get; set; }
        public long IdentificadorCaracteristica { get; set; }
    }
}
