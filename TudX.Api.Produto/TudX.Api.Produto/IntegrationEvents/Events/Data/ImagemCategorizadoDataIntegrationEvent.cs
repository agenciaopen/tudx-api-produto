﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TudX.Api.Produto.IntegrationEvents.Events.Data
{
    public class ImagemCategorizadoDataIntegrationEvent
    {
        public long Identificador { get; set; }
        public string UrlImagem { get; set; }
    }
}
