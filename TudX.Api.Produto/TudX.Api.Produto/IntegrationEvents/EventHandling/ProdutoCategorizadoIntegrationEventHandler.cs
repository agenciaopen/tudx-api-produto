﻿using Mapster;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TudX.Api.Produto.Application.Services;
using TudX.Api.Produto.CrossCuting.Data;
using TudX.Api.Produto.Domain.Model;
using TudX.Api.Produto.IntegrationEvents.Events;
using TudX.Domain.Base.Repository;
using TudX.IntegrationEvents.Application.Service;
using TudX.IntegrationEvents.EventBus.Abstractions;

namespace TudX.Api.Produto.IntegrationEvents.EventHandling
{
    public class ProdutoCategorizadoIntegrationEventHandler : IIntegrationEventHandler<ProdutoCategorizadoIntegrationEvent>
    {
        private readonly ProdutoService produtoService;
        private readonly EventoRecebidoService eventoRecebidoService;
        private readonly ILogger _logger;

        public ProdutoCategorizadoIntegrationEventHandler(IServiceProvider serviceProvider,
                                                          ILoggerFactory loggerFactory,
                                                          IMediator mediator)
        {
            produtoService = new ProdutoService(serviceProvider, mediator);
            eventoRecebidoService = new EventoRecebidoService(serviceProvider);
            _logger = loggerFactory?.CreateLogger<ProdutoCategorizadoIntegrationEventHandler>() ?? throw new ArgumentNullException(nameof(loggerFactory));
        }

        public async Task Handle(ProdutoCategorizadoIntegrationEvent @event)
        {
            try
            {
                await produtoService.ReceberProdutoCategorizado(@event.Adapt<ProdutoCategorizadoData>());
                eventoRecebidoService.RegistrarRecebimentoDeEvento(@event);
            }
            catch (Exception ex)
            {
                _logger.LogError(string.Format("Ocorreu um erro ao processar a mensagem: {0} Stack: {1}", ex.Message, ex.StackTrace));
            }
        }
    }
}
