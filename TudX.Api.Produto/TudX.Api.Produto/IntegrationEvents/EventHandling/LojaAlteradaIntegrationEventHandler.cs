﻿using Mapster;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TudX.Api.Produto.Application.Services;
using TudX.Api.Produto.CrossCuting.Data;
using TudX.Api.Produto.IntegrationEvents.Events;
using TudX.IntegrationEvents.Application.Service;
using TudX.IntegrationEvents.EventBus.Abstractions;

namespace TudX.Api.Produto.IntegrationEvents.EventHandling
{
    public class LojaAlteradaIntegrationEventHandler : IIntegrationEventHandler<LojaAlteradaIntegrationEvent>
    {
        private readonly LojaService lojaService;
        private readonly EventoRecebidoService eventoRecebidoService;
        private readonly ILogger _logger;

        public LojaAlteradaIntegrationEventHandler(IServiceProvider serviceProvider,
                                                   ILoggerFactory loggerFactory)
        {
            lojaService = new LojaService(serviceProvider);
            eventoRecebidoService = new EventoRecebidoService(serviceProvider);
            _logger = loggerFactory?.CreateLogger<CargaCategoriaAdicionadaIntegrationEventHandler>() ?? throw new ArgumentNullException(nameof(loggerFactory));
        }

        public async Task Handle(LojaAlteradaIntegrationEvent @event)
        {
            try
            {
                lojaService.InserirLoja(@event.Adapt<LojaIntegrationEventData>());
                eventoRecebidoService.RegistrarRecebimentoDeEvento(@event);
            }
            catch (Exception ex)
            {
                _logger.LogError(string.Format("Ocorreu um erro ao processar a mensagem: {0} Stack: {1}", ex.Message, ex.StackTrace));
            }
        }
    }
}
