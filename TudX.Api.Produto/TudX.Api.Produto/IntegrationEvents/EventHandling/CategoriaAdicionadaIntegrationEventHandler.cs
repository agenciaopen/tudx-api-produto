﻿using Mapster;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TudX.Api.Produto.Application.Services;
using TudX.Api.Produto.CrossCuting.Data;
using TudX.Api.Produto.IntegrationEvents.Events;
using TudX.IntegrationEvents.Application.Service;
using TudX.IntegrationEvents.EventBus.Abstractions;

namespace TudX.Api.Produto.IntegrationEvents.EventHandling
{
    public class CategoriaAdicionadaIntegrationEventHandler : IIntegrationEventHandler<CategoriaAdicionadaIntegrationEvent>
    {
        private readonly CategoriaService categoriaService;
        private readonly EventoRecebidoService eventoRecebidoService;
        private readonly ILogger _logger;

        public CategoriaAdicionadaIntegrationEventHandler(IServiceProvider serviceProvider,
                                                          ILoggerFactory loggerFactory)
        {
            categoriaService = new CategoriaService(serviceProvider);
            eventoRecebidoService = new EventoRecebidoService(serviceProvider);
            _logger = loggerFactory?.CreateLogger<CategoriaAdicionadaIntegrationEventHandler>() ?? throw new ArgumentNullException(nameof(loggerFactory));
        }

        public async Task Handle(CategoriaAdicionadaIntegrationEvent @event)
        {
            try
            {
                categoriaService.InserirCategoria(@event.Adapt<CategoriaIntegrationEventData>());
                eventoRecebidoService.RegistrarRecebimentoDeEvento(@event);
            }
            catch (Exception ex)
            {
                _logger.LogError(string.Format("Ocorreu um erro ao processar a mensagem: {0} Stack: {1}", ex.Message, ex.StackTrace));
            }
        }
    }
}
