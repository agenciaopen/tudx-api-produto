﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mapster;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TudX.Api.Base.Controller;
using TudX.Api.Produto.Application.Services;
using TudX.Api.Produto.CrossCuting.Data;
using TudX.Api.Produto.Domain.Model;
using TudX.Core;
using TudX.Domain.Base.Repository;
using TudX.Exceptions;

namespace TudX.Api.Produto.Controllers
{
    /// <summary>
    /// Fornece funcionalidades referentes a entidade Produto.
    /// </summary>
    [ApiVersion("1.0")]
    public class ProdutoController : TudxApiController
    {
        private ProdutoService produtoService;


        /// <summary>
        /// Construtor padrão.
        /// </summary>
        public ProdutoController(IServiceProvider serviceProvider, IMediator mediator)
        {
            produtoService = new ProdutoService(serviceProvider, mediator);
        }

        /// <summary>
        /// Recupera os produtos de acordo com o filtro.
        /// </summary>
        /// <param name="filtro">Filtro de produtos</param>
        /// <response code="200">Dados de todos os produtos</response>
        /// <response code="400">Dados de erro de negócio</response>
        /// <response code="500">Dados de erro de framework</response>
        [HttpPost]
        [ProducesResponseType(typeof(BaseResultPagination<ProdutoListagemData, ProdutoFiltroData>), 200)]
        [ProducesResponseType(typeof(BusinessException), 400)]
        [ProducesResponseType(typeof(Exception), 500)]
        public async Task<IActionResult> BuscarProdutos([FromBody] BaseFilterPagination<ProdutoFiltroData> filtro)
        {
            var produtos = produtoService.BuscarProdutosParaListagem(filtro);

            return Ok(produtos);
        }

        /// <summary>
        /// Recupera o produto com o identificador passado como parâmetro.
        /// </summary>
        /// <param name="identificador">Identificador da loja desejada.</param>
        /// <response code="200">Dados da lojas/response>
        /// <response code="400">Dados de erro de negócio</response>
        /// <response code="500">Dados de erro de framework</response>
        [HttpGet]
        [Route("{identificador}")]
        [ProducesResponseType(typeof(ProdutoDetalheData), 200)]
        [ProducesResponseType(typeof(BusinessException), 400)]
        [ProducesResponseType(typeof(Exception), 500)]
        public async Task<IActionResult> BuscarPorIdentificador(long identificador)
        {
            ProdutoDetalheData produto = await produtoService.BuscarProdutoPorIdentificador(identificador);

            return Ok(produto.Adapt<ProdutoDetalheData>());
        }

        /// <summary>
        /// Recupera o produto com o identificador passado como parâmetro.
        /// </summary>
        /// <param name="identificador">Identificador da loja desejada.</param>
        /// <response code="200">Dados da lojas/response>
        /// <response code="400">Dados de erro de negócio</response>
        /// <response code="500">Dados de erro de framework</response>
        [HttpGet]
        [Route("autocomplete/{nome}")]
        [ProducesResponseType(typeof(List<string>), 200)]
        [ProducesResponseType(typeof(BusinessException), 400)]
        [ProducesResponseType(typeof(Exception), 500)]
        public async Task<IActionResult> BuscarPorNomeParaAutoComplete(string nome)
        {
            List<string> termos = produtoService.BuscarTermosDeProdutosJaConsultados(nome);

            return Ok(termos);
        }
    }
}